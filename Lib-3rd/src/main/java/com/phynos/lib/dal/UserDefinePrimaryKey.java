package com.phynos.lib.dal;

public interface UserDefinePrimaryKey {

	public String getColumnName();
	
	public PropertyType getDataType();
	
	public String getSequenceName();
}
