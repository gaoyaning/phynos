package com.phynos.lib.dal;

public interface UserDefineColumn {

	public String getColumnName();
	
	public PropertyType getDataType();
	
	public int getSize();
}
