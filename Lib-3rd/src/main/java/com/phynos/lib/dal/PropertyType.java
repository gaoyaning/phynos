package com.phynos.lib.dal;

public enum PropertyType {
	
	Boolean, 
	Integer, 
	Long, 
	Float, 
	Double, 
	DateTime, 
	String,
	Binary
}
