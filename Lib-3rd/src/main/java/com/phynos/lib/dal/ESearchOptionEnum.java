package com.phynos.lib.dal;

public enum ESearchOptionEnum {
	LessThan, LessThanEquals, Equals, GreatThan, GreatThanEquals, NotEquals, 
	IsNull, IsNotNull, 
	In, NotIn,
	StartWith, EndWidth, Like, 
	LockTable
}
