package com.phynos.cache.redis;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;


/**
 * redis驱动管理类
 * @author lupc
 *
 */
public class RedisDriver {

	public static final String CONFILE_FILE = "/jedis.properties";

	private static final Logger LOG = Logger.getLogger(RedisDriver.class);

	private static JedisPool pool = null;

	private static int parseInt(String text, int defaultValue){
		if(text == null)
			return defaultValue;
		try{
			int result = Integer.parseInt(text);
			if(result <= 0)
				return defaultValue;
			else
				return result;
		}catch(Exception ex){
			return defaultValue;
		}
	}

	private static JedisPool getPool() { 		
		if (pool == null) {
			InputStream is = null;
			try {
				is = RedisDriver.class.getResourceAsStream(CONFILE_FILE);
				Properties ps = new Properties();
				ps.load(is);
				//读取配置
				int timeout = parseInt(ps.getProperty("timeout"),2000);
				int port = parseInt(ps.getProperty("jedisPort"),2000);
				String host = ps.getProperty("jedisHost","127.0.0.1");
				String password = ps.getProperty("password");
				int maxTotal = parseInt(ps.getProperty("maxTotal"),50);
				int maxIdle = parseInt(ps.getProperty("minIdle"),5);
				int maxWaitMillis = 1000 * parseInt(ps.getProperty("maxWaitSeconds"),10);
				//初始化redis配置
				JedisPoolConfig config = new JedisPoolConfig();
				config.setMaxTotal(maxTotal);
				config.setMaxIdle(maxIdle);
				config.setMaxWaitMillis(maxWaitMillis);
				//在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
				config.setTestOnBorrow(true);
				//
				if(password == null || password.equals("") || password.length() == 0){
					pool = new JedisPool(config, host, port, timeout);
				} else {
					pool = new JedisPool(config, host, port, timeout,password);
				}
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			} finally {
				if(is != null){
					try {
						is.close();
					} catch (Exception e2) {
						LOG.error(e2.getMessage(), e2);
					}
				}
			}					
		}
		return pool;
	}

	public static void releasePool(){
		if(pool != null){
			pool.destroy();	
		}    	
	}

	public static void setData(String key,String field,String json){
		Jedis jedis = null;    	
		try {
			jedis = getPool().getResource();
			jedis.hset(key, field, json);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} finally {
			if(jedis != null){
				jedis.close();	
			}			
		}
	}

	public static String getData(String key,String field){
		String result = null;
		Jedis jedis = null;    	
		try {
			jedis = getPool().getResource();
			result = jedis.hget(key, field);
			LOG.debug(result);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} finally {
			if(jedis != null){
				jedis.close();	
			}
		}
		return result;
	}
	
	public static Long delField(String key,String field){
		Long result = null;
		Jedis jedis = null;    	
		try {
			jedis = getPool().getResource();
			result = jedis.hdel(key, field);			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} finally {
			if(jedis != null){
				jedis.close();	
			}
		}
		return result;
	}

	public static void main(String[] args){

		Map<String, String> map = new HashMap<>();  
		map.put( "1", "aaa" );  
		map.put( "2", "bbb" );  
		map.put( "3", "ccc" );  
		map.put( "4", "ddd" );  
		map.put( "5", "eee" );  
		map.put( "6", "fff" );  
		map.put( "7", "ggg" );  
		map.put( "8", "hhh" );  				

		LOG.debug(map);
	}

}