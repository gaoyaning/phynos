跳过测试阶段：
mvn package -DskipTests

临时性跳过测试代码的编译：
mvn package -Dmaven.test.skip=true
maven.test.skip同时控制maven-compiler-plugin和maven-surefire-plugin两个插件的行为，即跳过编译，又跳过测试。


## maven使用的一些关键注意点
- 编码问题
- jdk版本问题


## maven常用功能
- 利用profile实现特殊构建


## maven应用常见问题
- 无法调试问题