## spring必会知识点
- 如何实例化一个Bean
- ApplicationContext和BeanFactory有什么区别
- 如何通过Spring读取外部文件
- Spring配置文件中，Bean的id是否可以重复
- 解释Spring中的自动装配和类型检查
- 当系统中存在多个Spring配置文件，ApplicationContext可否一次全部加载？如果其中的id有重复应该如何处理
- 什么是AOP？AOP中涉及到哪些术语并进行解释
- Spring是如何实现AOP的？详细描述动态代理

## spring各个包
- core
- bean
- context
- expression
- AOP
- web
- webMVC
- JDBC
- spring-tx

## spring关键知识点
- spring设计哲学
- spring事务
- spring注解

## spring中bean的生命周期
- 实例化。Spring通过new关键字将一个Bean进行实例化，JavaBean都有默认的构造函数，因此不需要提供构造参数。
- 填入属性。Spring根据xml文件中的配置通过调用Bean中的setXXX方法填入对应的属性。
- 事件通知。Spring依次检查Bean是否实现了BeanNameAware、BeanFactoryAware、ApplicationContextAware、BeanPostProcessor、InitializingBean接口，如果有的话，依次调用这些接口。
- 使用。
- 销毁。如果Bean实现了DisposableBean接口，就调用其destroy方法。

## spring常用注解
@Configuration把一个类作为一个IoC容器，它的某个方法头上如果注册了@Bean，就会作为这个Spring容器中的Bean。
- @Scope注解 作用域
@Lazy(true) 表示延迟初始化
- @Service用于标注业务层组件、 
- @Controller用于标注控制层组件（如struts中的action）
@Repository用于标注数据访问组件，即DAO组件。
@Component泛指组件，当组件不好归类的时候，我们可以使用这个注解进行标注。
- @Scope用于指定scope作用域的（用在类上）
@PostConstruct用于指定初始化方法（用在方法上）
@PreDestory用于指定销毁方法（用在方法上）
@DependsOn：定义Bean初始化及销毁时的顺序
@Primary：自动装配时当出现多个Bean候选者时，被注解为@Primary的Bean将作为首选者，否则将抛出异常
- @Autowired 默认按类型装配，如果我们想使用按名称装配，可以结合@Qualifier注解一起使用。如下：
- @Autowired @Qualifier("personDaoBean") 存在多个实例配合使用
- @Resource 默认按名称装配，当找不到与名称匹配的bean才会按类型装配。（J2EE注解）
@PostConstruct 初始化注解
@PreDestroy 摧毁注解 默认 单例  启动就加载
@Async异步方法调用
