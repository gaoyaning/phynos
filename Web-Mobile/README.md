# 移动端后台（包括APP，微信公众号）技术规格
- 基于springMVC 的Restful风格API接口
- 基于gson的json序列化
- 支持第三方访问的outh2协议
- 基于mybatis的数据库访问
- 缓存

##注意事项
日志：基于log4j-1.2版本，注意与2.0版本的区别