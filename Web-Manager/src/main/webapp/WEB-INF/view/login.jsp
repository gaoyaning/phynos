<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>登录界面</title>
	<!-- jquery -->
	<script type="text/javascript" src="<%=path%>/include/js/jquery-3.2.0.min.js"></script>
	<!-- bootstrap -->
	<link rel="stylesheet" href="<%=path%>/include/lib/bootstrap-3.3.7-dist/css/bootstrap.min.css">
	<script type="text/javascript" src="<%=path%>/include/lib/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	<!-- 自定义js -->
<link rel="stylesheet" href="<%=path%>/include/css/login.css">
</head>
<body>
	<div class="container">

      <form class="form-signin" action="index" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->
</body>
</html>