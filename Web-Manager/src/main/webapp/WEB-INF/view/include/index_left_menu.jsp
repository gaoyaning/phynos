<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>	
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


          <ul class="nav nav-sidebar">
            <li class="active"><a href="#">Overview <span class="sr-only">(current)</span></a></li>
            <li><a href="#">Reports</a></li>
            <li><a href="#">Analytics</a></li>
            <li><a href="#">Export</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="">Nav item</a></li>
            <li><a href="">Nav item again</a></li>
            <li><a href="">One more nav</a></li>
            <li><a href="">Another nav item</a></li>
            <li>
            	<a href="#" data-toggle="collapse" data-target="#form_demo">表单例子</a>
            	<ul id="form_demo" class="panel-collapse collapse">
            		<li>
            			<a href="<%=path%>/form/form1">表单提交1</a>
            		</li>
            		<li>
            			<a href="#">表单提交2</a>
            		</li>
            		<li>
            			<a href="#">表单提交3</a>
            		</li>
            	</ul>
            </li>
          </ul>
          
          <ul class="nav nav-sidebar">
            <li>
            	<a href="#" data-toggle="collapse" data-target="#collapseListGroup1" role="tab">战国三雄</a>
            	<ul id="collapseListGroup1" class="panel-collapse collapse">
            		<li>
            			<a href="#">织田信长</a>
            		</li>
            		<li>
            			<a href="#">丰臣秀吉</a>
            		</li>
            		<li>
            			<a href="#">德川家康</a>
            		</li>
            	</ul>
            </li>
            <li><a href="">One more nav</a></li>
            <li><a href="">Another nav item</a></li>
          </ul>