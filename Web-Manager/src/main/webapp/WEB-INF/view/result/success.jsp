<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>操作成功</title>
	<!-- jquery -->
	<script type="text/javascript" src="<%=path%>/include/js/jquery-3.2.0.min.js"></script>
	<!-- bootstrap -->
	<link rel="stylesheet" href="<%=path%>/include/lib/bootstrap-3.3.7-dist/css/bootstrap.min.css">
	<script type="text/javascript" src="<%=path%>/include/lib/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	<!-- 自定义js -->
</head>
<body>

</body>
</html>