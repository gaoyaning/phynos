<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>文件下载</title>
</head>
<body>
	<div>静态资源：<a></a></div>	
	<div>txt：<a href="txt">b.txt</a></div>
	<div>Excel：<a href="excel">b.xls</a></div>
	<div>xml：<a href="xml">b.xml</a></div>
	<div>json：<a href="json">b.json</a></div>
	<div>pdf：<a href="pdf">b.pdf</a></div>
	<div>受限资源：<a></a></div>
</body>
</html>