<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>	
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>系统主页</title>
    <!-- jquery -->
    <script type="text/javascript" src="<%=path%>/include/js/jquery-3.2.0.min.js"></script>
    <!-- bootstrap -->  
    <link rel="stylesheet" href="<%=path%>/include/lib/bootstrap-3.3.7-dist/css/bootstrap.min.css" >
    <script type="text/javascript" src="<%=path%>/include/lib/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <!-- 自定义js -->    
    <link rel="stylesheet" href="<%=path%>/include/css/index.css" >
</head>
<body>

	<!-- 头部 -->
	<jsp:include page="../include/index_head.jsp"></jsp:include>

    <div class="container-fluid">
      <div class="row">
        <!-- 左边导航栏 -->
        <div class="col-sm-3 col-md-2 sidebar">
        	<jsp:include page="../include/index_left_menu.jsp"></jsp:include>
        </div>
        
        <!-- 右边内容栏 -->
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2">                 
          <ol class="breadcrumb">
			  <li><a href="#">Home</a></li>
			  <li><a href="#">2013</a></li>
			  <li class="active">十一月</li>
		  </ol>
		<form class="form-horizontal" role="form">
		  <div class="form-group">
		    <label for="firstname" class="col-sm-3 control-label no-padding-right">名字</label>
		    <div class="col-sm-3">
		      	<input type="text" class="form-control" id="firstname" placeholder="请输入名字">
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="lastname" class="col-sm-2 control-label">姓</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="lastname" placeholder="请输入姓">
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <div class="checkbox">
		        <label>
		          <input type="checkbox">请记住我
		        </label>
		      </div>
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <button type="submit" class="btn btn-default">登录</button>
		    </div>
		  </div>
		  <div class="dropdown">
		    <button type="button" class="btn dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown">主题
		        <span class="caret"></span>
		    </button>
		    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
		          <li role="presentation">
		              <a role="menuitem" tabindex="-1" href="#">Java</a>
		          </li>
		          <li role="presentation">
		              <a role="menuitem" tabindex="-1" href="#">数据挖掘</a>
		          </li>
		          <li role="presentation">
		              <a role="menuitem" tabindex="-1" href="#">数据通信/网络</a>
		          </li>
		          <li role="presentation" class="divider"></li>
		          <li role="presentation">
		              <a role="menuitem" tabindex="-1" href="#">分离的链接</a>
		         </li>
		      </ul>
		  </div>
		</form>

        </div>     
      
      </div>
    </div>   	
</body>
</html>
