<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>	
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>系统主页</title>
    <!-- jquery -->
    <script type="text/javascript" src="<%=path%>/include/js/jquery-3.2.0.min.js"></script>
    <!-- bootstrap -->  
    <link rel="stylesheet" href="<%=path%>/include/lib/bootstrap-3.3.7-dist/css/bootstrap.min.css" >
    <script type="text/javascript" src="<%=path%>/include/lib/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <!-- 自定义js -->    
    <link rel="stylesheet" href="<%=path%>/include/css/index.css" >
</head>
<body>

	<!-- 头部 -->
	<jsp:include page="include/index_head.jsp"></jsp:include>

    <div class="container-fluid">
      <div class="row">
        <!-- 左边导航栏 -->
        <div class="col-sm-3 col-md-2 sidebar">
        	<jsp:include page="include/index_left_menu.jsp"></jsp:include>
        </div>
        
        <!-- 右边内容栏 -->
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        	<jsp:include page="include/index_content.jsp"></jsp:include>      
        </div>
      </div>
    </div>   	
</body>
</html>
