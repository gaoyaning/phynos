package com.phynos.web.manager.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/form")
public class Form {

	@RequestMapping("/form1")
	public String hello(){
		return "form/form1";
	}
	
}
