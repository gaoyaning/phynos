package com.phynos.web.manager.demo;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.phynos.lib.common.captcha.CaptchaUtil;

@Controller
@RequestMapping("/demo")
public class Common {

	@RequestMapping(value="/common",method=RequestMethod.GET)
	public String getCommonView(){
		return "demo/common";
	}
	
	
	
}
