package com.phynos.web.manager;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 一些 应用级 脚本/样式文件 的版本管理（防止缓存）
 * @author lupc
 *
 */
public class MyVersion {

	private static final String CONFIG_FILE = "/version.properties";
	
	/**
	 * public.js版本号
	 */
	public static final String JS_MY_PUBLIC;
	
	/**
	 * public.css版本号
	 */
	public static final String CSS_MY_PUBLIC; 
	
	static {
		Properties props = getConfig();
		
		JS_MY_PUBLIC = props.getProperty("JS_MY_PUBLIC", "1.0.0");
		
		CSS_MY_PUBLIC = props.getProperty("CSS_MY_PUBLIC", "1.0.0");
	}
	
	static Properties getConfig() {
		Properties prop = new Properties(); 
		InputStream in = null;
		in = MyVersion.class.getResourceAsStream(CONFIG_FILE);
		try {
			prop.load(in);
		} catch (IOException e) {		
			e.printStackTrace();
		} finally {
			if(in != null){
        		try {
        			in.close();
				} catch (Exception e2) {
				
				}
        	}
		}
		return prop;
	}
		
}
