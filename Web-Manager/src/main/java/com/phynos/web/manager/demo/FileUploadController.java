package com.phynos.web.manager.demo;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/demo")
public class FileUploadController {

	@RequestMapping(value="/upload",method=RequestMethod.GET)
	public String uploadFile(){
		return "demo/upload";
	}
	
	@RequestMapping(value="/upload",method=RequestMethod.POST)
	public String login(
			@RequestParam MultipartFile[] myfiles, 
			HttpServletRequest request			
			){
		return "result/success";
	}
	
}
