package com.phynos.web.manager.demo;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/demo")
public class FileDownload {

	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public String downloadFile() {
		return "demo/download";
	}

	@RequestMapping(value = "/txt", method = RequestMethod.GET)
	public void downloadTxt(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		byte[] data = {1 + '0',6 + '0'};
		String fileName = "b.txt";
		fileName = URLEncoder.encode(fileName, "UTF-8");
		response.reset();
		response.setHeader("Content-Disposition", "attachment; filename=\""
				+ fileName + "\"");
		response.addHeader("Content-Length", "" + data.length);
		response.setContentType("application/octet-stream;charset=UTF-8");
		OutputStream outputStream = new BufferedOutputStream(
				response.getOutputStream());
		outputStream.write(data);
		outputStream.flush();
		outputStream.close();
	}

}
