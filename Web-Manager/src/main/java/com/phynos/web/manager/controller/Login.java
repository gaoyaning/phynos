package com.phynos.web.manager.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.phynos.lib.common.captcha.CaptchaUtil;

@Controller
public class Login {
		
	@GetMapping
	@RequestMapping(value="/login",method=RequestMethod.GET)
	public String getLoginView(){
		return "login";
	}
	
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public String login(
			@RequestParam("name") String name
			){
		return "redirect:index";
	}
	
	@RequestMapping(value="/captcha",method=RequestMethod.GET)
	@ResponseBody
	public void capture(
			HttpServletRequest req, 
			HttpServletResponse res) throws IOException{
		String code = CaptchaUtil.createCaptureImage(res.getOutputStream());
		System.out.println(code);
		res.getOutputStream().flush();
	}

}
