package com.phynos.web.manager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.phynos.dal.mapper.UserMapper;
import com.phynos.dal.model.User;

@Service
@Transactional
public class UserService {
	
	public User getUser(long id) {
        return userMapper.getUser(id);
    }
     
    @Autowired
    private UserMapper userMapper;
    
}
