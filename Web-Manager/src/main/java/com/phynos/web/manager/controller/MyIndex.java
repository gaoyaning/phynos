package com.phynos.web.manager.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class MyIndex {
	
	@RequestMapping("/")
	public String getIndex(){
		return "redirect:/index";
	}
	
	@RequestMapping("/index")
	public String hello(){
		return "index";
	}
	
	@RequestMapping("/lang")
	public String getLang(){
		return "lang";
	}
	
}
