# 应用后台管理（网页端）
管理系统的使用场景主要是专业人员，对浏览器的兼容性要求不高


## WEB前端最小系统
- bootstrap
- echarts
- My97
- colorbox
- 富文本编辑

## WEB后台最小系统：
- 登陆控制
- 静态资源
- 动态页面(传值，jsp标签，spring标签)
- 动态返回text,json,xml,excel,pdf
- 文件上次
- 文件下载
- 动态图片（验证码）
- 国际化（多语言）（资源文件，数据库）
- 日志系统
- 数据库接口
ref:http://hmkcode.com/spring-mvc-view-json-xml-pdf-or-excel/

## 配合
- websocket

## 涉及到的类库
- 数据层采用mybatis
- 后端框架采用springMVC
- 后端文档处理POI
- 后端其他包括日志(log4j)
