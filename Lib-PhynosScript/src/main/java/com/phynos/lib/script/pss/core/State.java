package com.phynos.lib.script.pss.core;

/**
 * 脚本语言执行上下文
 * <p>虚拟机中的环境表、注册表、运行堆栈、虚拟机的上下文等数据</p>
 * @author lupc
 *
 */
public class State {
    
	//256K
    public static final int POOL_SIZE = 256 * 1024;

    private boolean debug;

    private byte[] a1;                  //symbol area，符号区
    private byte[] a2;                  //text area，代码区
    private byte[] a3;                  //data area，数据区
    private byte[] a4;                  //stack area，堆栈区

    public State()
    {
        //symbol area
        a1 = new byte[POOL_SIZE];
        //text area
        a2 = new byte[POOL_SIZE];
        //data area
        a3 = new byte[POOL_SIZE];
        //stack area
        a4 = new byte[POOL_SIZE];
    }

    public void loadFile(String file)
    { 
       
    }
    
}
