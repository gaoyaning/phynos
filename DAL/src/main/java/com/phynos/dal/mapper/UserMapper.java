package com.phynos.dal.mapper;

import org.apache.ibatis.annotations.Param;

import com.phynos.dal.model.User;

public interface UserMapper {

	User getUser(@Param("id") long id);

	User getUsersByUserName(@Param("userName") String userName);
	
	int addUser(User user);
	
	int updateUser(User user);
	
	int deleteUser(@Param("id") long id);
}
