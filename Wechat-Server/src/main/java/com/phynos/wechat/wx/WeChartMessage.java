package com.phynos.wechat.wx;

import java.util.Date;

public class WeChartMessage {

	/**
	 * 确认收到微信发送过来的消息，但是不想回复或5秒内无法回复，则必须回复此消息
	 */
	public static final String MSG_SUCCESS = "success";
	
	public static final String MSG_TYPE_TEXT = "text";	
	public static final String MSG_TYPE_EVENT = "event";
	
	public static final String MSG_EVENT_SUBSCRIBE = "subscribe";	
	public static final String MSG_EVENT_UNSUBSCRIBE = "unsubscribe";
	public static final String MSG_EVENT_CLICK = "CLICK";	
	
	public static String responseText(String custermname,String servername,String text){
		StringBuffer sb = new StringBuffer();
		sb.append("<xml>");  
		sb.append("<ToUserName><![CDATA[" + custermname + "]]></ToUserName>");  
		sb.append("<FromUserName><![CDATA[" + servername + "]]></FromUserName>");  
		sb.append("<CreateTime>" + new Date().getTime() + "</CreateTime>");  
		sb.append("<MsgType><![CDATA[" + MSG_TYPE_TEXT + "]]></MsgType>");  
		sb.append("<Content><![CDATA[" + text + "]]></Content>");  
        sb.append("</xml>"); 
		return sb.toString();
	}
			

}
