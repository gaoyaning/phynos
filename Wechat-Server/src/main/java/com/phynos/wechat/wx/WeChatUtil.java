package com.phynos.wechat.wx;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;

/**
 * 微信公众号开发相关的工具类
 * @author lupc
 *
 */
public class WeChatUtil {
	
	private static final Logger LOG = Logger.getLogger(WeChatUtil.class);
	
	public static String SHA1(String decript) {  
		try {  
			MessageDigest digest = java.security.MessageDigest.getInstance("SHA-1");  
			digest.update(decript.getBytes());  
			byte messageDigest[] = digest.digest();  
			// Create Hex String  
			StringBuffer hexString = new StringBuffer();  
			// 字节数组转换为 十六进制 数  
			for (int i = 0; i < messageDigest.length; i++) {  
				String shaHex = Integer.toHexString(messageDigest[i] & 0xFF);  
				if (shaHex.length() < 2) {  
					hexString.append(0);  
				}  
				hexString.append(shaHex);  
			}  
			return hexString.toString();  

		} catch (NoSuchAlgorithmException e) {  
			LOG.error(e.getMessage(), e);  
		}  
		return "";  
	} 
	
}
