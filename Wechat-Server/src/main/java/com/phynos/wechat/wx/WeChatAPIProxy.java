package com.phynos.wechat.wx;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 微信http api代理类
 * @author lupc
 *
 */
public class WeChatAPIProxy {

	/**
	 * 获取token
	 */
	public static final String URL_GET_TOKEN = "https://api.weixin.qq.com/cgi-bin/token";
	
	public static final String URL_GET_TICKET = "https://api.weixin.qq.com/cgi-bin/ticket/getticket";
	
	public static final String URL_GET_USER_INFO = "https://api.weixin.qq.com/cgi-bin/user/info";
	
	public static final String URL_SEND_TEMPLATE_MSG = "https://api.weixin.qq.com/cgi-bin/message/template/send";
	
	/**
	 * 
	 */
	public static final String URL_MENU_CREATE = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";
	/**
	 * 
	 */
	public static final String URL_MENU_DELETE = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=ACCESS_TOKEN";
	/**
	 * 
	 */
	public static final String URL_MENU_GET = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=ACCESS_TOKEN";

	
	public static final int TIMEOUT_READ = 12;
	public static final int TIMEOUT_WRITE = 6;
	public static final int TIMEOUT_CONNECT = 3;
		
	protected static String httpRequestString(Request request) throws IOException {
		OkHttpClient client =  new OkHttpClient.Builder()
		.readTimeout(TIMEOUT_READ,TimeUnit.SECONDS)//设置读取超时时间
		.writeTimeout(TIMEOUT_WRITE,TimeUnit.SECONDS)//设置写的超时时间
		.connectTimeout(TIMEOUT_CONNECT,TimeUnit.SECONDS)//设置连接超时时间
		.build();
		Response response = client.newCall(request).execute();  
		if (!response.isSuccessful())  
			throw new IOException("Unexpected code " + response);  
		return response.body().string();
	}


	public static String getAccessToken(String appId,String appSecret) throws IOException{
		String url = String.format(
				"%s?grant_type=%s&appid=%s&secret=%s",
				URL_GET_TOKEN,
				"client_credential",
				appId,
				appSecret);
		Request request = new Request.Builder()  
		.url(url) 		
		.build(); 
		String json = httpRequestString(request);
		return json;
	}

	public static String getTicket(String accessToken) throws IOException{
		String url = String.format(
				"%s?access_token=%s&type=jsapi",
				URL_GET_TICKET,
				accessToken);
		Request request = new Request.Builder()  
		.url(url) 		
		.build(); 
		String json = httpRequestString(request);		
		return json;
	}

	/**
	 * 获取微信用户信息
	 * @param accessToken
	 * @param openid
	 * @return
	 * @throws IOException
	 */
	public static String getWxUserInfo(String accessToken,String openid) throws IOException{
		String result = "";
		String url = String.format(
				"%s?access_token=%s&openid=%s&lang=zh_CN",
				URL_GET_USER_INFO,
				accessToken,
				openid);
		Request request = new Request.Builder()
		.url(url)
		.build();
		result = httpRequestString(request);		
		return result;
	}
	
	private static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");
	
	/**
	 * 发送模板消息
	 * @param accessToken
	 * @param msg
	 * @return
	 * @throws IOException
	 */
	public static String sendTemplateMsg(String accessToken,String msg) throws IOException{
		String url = String.format(
				"%s?access_token=%s",
				URL_SEND_TEMPLATE_MSG,
				accessToken);
		okhttp3.RequestBody body = okhttp3.RequestBody
				.create(MEDIA_TYPE_JSON, msg);
		Request request = new Request.Builder()  
		.url(url) 
		.post(body)
		.build(); 
		httpRequestString(request);
		return "";
	}
	

	public void createMenu(String json){

	}

}
