package com.phynos.wechat.wx;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 微信推送过来的xml文本转化为此消息
 * @author lupc
 *
 */
@XStreamAlias("xml")  
public class WeChatInputMessage implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4685708248062920925L;
	
	@XStreamAlias("ToUserName")  
    private String ToUserName;  
    @XStreamAlias("FromUserName")  
    private String FromUserName;  
    @XStreamAlias("CreateTime")  
    private Long CreateTime;  
    @XStreamAlias("MsgType")  
    private String MsgType = "text";  
    @XStreamAlias("MsgId")  
    private Long MsgId;  
    
    // 文本消息  
    @XStreamAlias("Content")  
    private String Content;  
    
    // 图片消息  
    @XStreamAlias("PicUrl")  
    private String PicUrl;  
    
    // 位置消息  
    @XStreamAlias("LocationX")  
    private String LocationX;  
    
    @XStreamAlias("LocationY")  
    private String LocationY;  
    
    @XStreamAlias("Scale")  
    private Long Scale;  
    
    @XStreamAlias("Label")  
    private String Label;  
    
    // 链接消息  
    @XStreamAlias("Title")  
    private String Title;  
    
    @XStreamAlias("Description")  
    private String Description;  
    
    @XStreamAlias("Url")  
    private String URL;  
    
    // 语音信息  
    @XStreamAlias("MediaId")  
    private String MediaId;  
    
    @XStreamAlias("Format")  
    private String Format;  
    
    @XStreamAlias("Recognition")  
    private String Recognition;  
    
    // 事件  
    @XStreamAlias("Event")  
    private String Event;  
    @XStreamAlias("EventKey")  
    private String EventKey;  
    @XStreamAlias("Ticket")  
    private String Ticket;
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getToUserName() {
		return ToUserName;
	}
	public String getFromUserName() {
		return FromUserName;
	}
	public Long getCreateTime() {
		return CreateTime;
	}
	public String getMsgType() {
		return MsgType;
	}
	public Long getMsgId() {
		return MsgId;
	}
	public String getContent() {
		return Content;
	}
	public String getPicUrl() {
		return PicUrl;
	}
	public String getLocationX() {
		return LocationX;
	}
	public String getLocationY() {
		return LocationY;
	}
	public Long getScale() {
		return Scale;
	}
	public String getLabel() {
		return Label;
	}
	public String getTitle() {
		return Title;
	}
	public String getDescription() {
		return Description;
	}
	public String getURL() {
		return URL;
	}
	public String getMediaId() {
		return MediaId;
	}
	public String getFormat() {
		return Format;
	}
	public String getRecognition() {
		return Recognition;
	}
	public String getEvent() {
		return Event;
	}
	public String getEventKey() {
		return EventKey;
	}
	public String getTicket() {
		return Ticket;
	}
	public void setToUserName(String toUserName) {
		ToUserName = toUserName;
	}
	public void setFromUserName(String fromUserName) {
		FromUserName = fromUserName;
	}
	public void setCreateTime(Long createTime) {
		CreateTime = createTime;
	}
	public void setMsgType(String msgType) {
		MsgType = msgType;
	}
	public void setMsgId(Long msgId) {
		MsgId = msgId;
	}
	public void setContent(String content) {
		Content = content;
	}
	public void setPicUrl(String picUrl) {
		PicUrl = picUrl;
	}
	public void setLocationX(String locationX) {
		LocationX = locationX;
	}
	public void setLocationY(String locationY) {
		LocationY = locationY;
	}
	public void setScale(Long scale) {
		Scale = scale;
	}
	public void setLabel(String label) {
		Label = label;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	public void setMediaId(String mediaId) {
		MediaId = mediaId;
	}
	public void setFormat(String format) {
		Format = format;
	}
	public void setRecognition(String recognition) {
		Recognition = recognition;
	}
	public void setEvent(String event) {
		Event = event;
	}
	public void setEventKey(String eventKey) {
		EventKey = eventKey;
	}
	public void setTicket(String ticket) {
		Ticket = ticket;
	}  
	    
}
