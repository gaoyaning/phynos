# 物联网系统技术架构
目前尚处于规划和基础代码整理阶段，目标是:一个现成的支持微信公众号、小程序、手机APP后台、用户网页、后台管理网页、
内容发布系统、电商、数据采集服务器等的综合系统。 

## 声明
本系统部分代码和类库都来自于互联网，我会尽量标明出处；如果有遗漏之处，请见谅。

## 功能结构
- 服务器-数据采集
- 客户端模拟器-自定义
- 服务器-数据采集-MQTT
- 客户端模拟器 -MQTT 
- 用户网页（网页）
- 管理后台系统（网页）
- 支付系统
- 订阅系统
- 移动后台
- 开放API
- 微信-服务器端（包含API接口和公众号网页）
- 微信-小程序（基于微信开发工具）
- Lib-自定义脚本语言
- Lib-第三方代码库
- Lib-工具库

## 使用其他工具的系统
- 内容发布系统-wordpress 
- 电商系统（网页）

## 技术选型

### 后端选型
- Spring
- SpringMVC
- Mybatis
- MyBatis Generator
- Freemarker
- Log4J
- okhttp
- Mina
- MQTT-eclipse.paho
- Maven

### 前端选型
- jquery
- bootstrap
- weui
- colorbox
- ztree

## 中间件
 - mysql
 - tomcat
 - mqtt代理服务器
 - redis缓存  
 
 ## 分布式和集群
 目前的规划还只是最小系统，分布式支持待定

*得套路者，得天下*